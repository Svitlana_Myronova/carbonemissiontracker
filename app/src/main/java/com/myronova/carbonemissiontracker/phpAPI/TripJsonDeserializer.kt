package com.myronova.carbonemissiontracker.phpAPI

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.myronova.carbonemissiontracker.tripDB.Trip
import java.lang.reflect.Type
import java.text.SimpleDateFormat

@Suppress("DEPRECATION")
class TripJsonDeserializer : JsonDeserializer<Trip> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Trip? {
        if(json == null)
            return null

        if(!json.isJsonObject)
            return null

        val jsonObj = json.asJsonObject

        val travelMode = jsonObj["mode"].asString

        val carEngine = if (jsonObj["engine"] != null) {
            jsonObj["engine"].asString
        } else {
            ""
        }

        val jsonFromObj = jsonObj["from"].asJsonObject
        val fromLatitude = jsonFromObj["latitude"].asFloat
        val fromLongitude = jsonFromObj["longitude"].asFloat

        val jsonToObj = jsonObj["from"].asJsonObject
        val toLatitude = jsonToObj["latitude"].asFloat
        val toLongitude = jsonToObj["longitude"].asFloat

        val distance = jsonObj["distance"].asFloat
        val co2Emission = jsonObj["co2emissions"].asFloat

        val df1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val dateStr = jsonObj["created_at"].asString
        val date = df1.parse(dateStr) ?: return null

        val travelTime = jsonObj["traveltime"].asFloat

        val id = jsonObj["id"].asInt
        return Trip(
            date,
            travelMode,
            carEngine,
            fromLatitude,
            fromLongitude,
            toLatitude,
            toLongitude,
            distance,
            co2Emission,
            travelTime,
            "",
            id)
    }
}