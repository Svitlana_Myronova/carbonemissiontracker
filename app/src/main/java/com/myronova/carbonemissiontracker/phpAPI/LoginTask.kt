package com.myronova.carbonemissiontracker.phpAPI

import android.os.AsyncTask
import android.util.Log
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

open class LoginTask : AsyncTask<String, Void, String?>() {

    // runs in background (not in UI thread)
    override fun doInBackground(vararg params: String): String? {
        // params comes from the execute() call: params[0] is the url.
        try {
            val baseUrl = params[0]
            val email = params[1]
            val password = params[2]
            return downloadUrl(baseUrl, email, password)
        } catch (e: IOException) {
            Log.e(TAG, "exception thrown by download " + e.message, e)
            return null;
        }
    }

    /*
* Given a URL, establishes an HttpUrlConnection and retrieves the web page
* content as a InputStream, which it returns as a string.
*/
    @Throws(IOException::class)
    private fun downloadUrl(baseUrl: String, email : String, password : String): String? {
        val loginURL = baseUrl + "/api/auth/login"
        Log.d(TAG, "downloadUrl $loginURL")

        var istream: InputStream? = null
        // Only read the first 500 characters of the retrieved
        // web page content.
        // int len = MAXBYTES;

        val url : URL
        try {
            url = URL(loginURL)
        } catch (e: MalformedURLException) {
            Log.d(TAG, e.message.toString())
            return null
        }

        var conn: HttpURLConnection? = null
        try {
            // create and open the connection
            conn = url.openConnection() as HttpURLConnection

            /*
             * set maximum time to wait for stream read read fails with
             * SocketTimeoutException if elapses before connection established
             *
             * in milliseconds
             *
             * default: 0 - timeout disabled
             */
            conn.readTimeout = 10000
            /*
             * set maximum time to wait while connecting connect fails with
             * SocketTimeoutException if elapses before connection established
             *
             * in milliseconds
             *
             * default: 0 - forces blocking connect timeout still occurs but
             * VERY LONG wait ~several minutes
             */
            conn.connectTimeout = 15000
            /*
             * HTTP Request method defined by protocol
             * GET/POST/HEAD/POST/PUT/DELETE/TRACE/CONNECT
             *
             * default: GET
             */
            conn.requestMethod = "POST"
            // specifies whether this connection allows receiving data
            conn.doInput = true
            // specifies whether this connection allows outgoing data
            conn.doOutput = true

            val loginData : String = "{ \"email\":\"$email\",\"password\":\"$password\" }"
            var loginDataBytes : ByteArray = loginData.toByteArray(charset("UTF-8"))

            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
            conn.addRequestProperty("Content-Length", loginDataBytes.size.toString())

            Log.d(TAG, "post "+loginDataBytes.size.toString())
            Log.d(TAG, "post "+loginDataBytes)

            //send the POST out
            val out = BufferedOutputStream(conn.outputStream)

            out.write(loginDataBytes)
            out.flush()
            out.close()

            val response = conn.responseCode
            Log.d(TAG, "Server returned: $response")

            /*
             *  check the status code HTTP_OK = 200 anything else we didn't get what
             *  we want in the data.
             */
            if (response != HttpURLConnection.HTTP_OK)
                return null;

            // get the stream for the data from the website
            istream = conn.inputStream
            // read the stream, returns String
            val responseStr = readIt(istream)

            val responseJson = JSONObject(responseStr)

            return responseJson.getString("access_token")

        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            Log.getStackTraceString(e)
            throw e
        } finally {
            /*
             * Make sure that the InputStream is closed after the app is
             * finished using it.
             * Make sure the connection is closed after the app is finished using it.
             */

            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {
                }

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {
                    }

            }
        }
    } // downloadUrl()

    /*
    * Reads stream from HTTP connection and converts it to a String.
    *
    *  We use a BufferedInputStream to read the data from the http connection InputStream
    *  into our buffer  (NETIOBUFFER bytes at a time, I often choose 1KiB for
    *  probably small datastreams.)
    *
    *  We then use a ByteArrayOutputStream + DataOutputStream.writer to collect the data
    *  and write it to a byte array.
    *  The reason this is done is that we do not know the length of the incoming data
    *  So the OutputStream components allow us to put data into and grow the buffer.
    *
    *  It is unfortunate that in Java everything is a pointer but we do not have access
    *  to the underlying structures.  If this is done in c or c++ we allocate, manage,
    *  grow and release the buffers as we need them.
    *
    *  In java using these classes has that effect.  It is not necessarily efficient,
    *  a better solution may be needed for large data exchange.
    */
    @Throws(IOException::class)
    fun readIt(stream: InputStream?): String {
        var bytesRead: Int
        var totalRead = 0
        val buffer = ByteArray(NETIOBUFFER)

        // for data from the server
        val bufferedInStream = BufferedInputStream(stream!!)
        // to collect data in our output stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(byteArrayOutputStream)

        // read the stream until end
        bytesRead = 0
        while (bytesRead != -1) {
            bytesRead = bufferedInStream.read(buffer)
            if (bytesRead > 0) {
                writer.write(buffer ,0, bytesRead)
                totalRead += bytesRead
            }
        }
        writer.flush()
        Log.d(TAG, "Bytes read: " + totalRead
                + "(-1 means end of reader so max of)")

        return byteArrayOutputStream.toString()
    } // readIt()

    companion object {
        private val TAG = "LoginTask"
        private val NETIOBUFFER = 1024
    }

}