package com.myronova.carbonemissiontracker.phpAPI

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.myronova.carbonemissiontracker.tripDB.Trip
import java.lang.reflect.Type


class TripJsonSerializer : JsonSerializer<Trip> {
    override fun serialize(
        trip: Trip?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement?
    {
        if(trip == null) {
            return null
        }

        val jsonTrip = JsonObject()

        jsonTrip.addProperty("mode", trip.travelMode)
        jsonTrip.addProperty("engine", trip.carEngine)
        jsonTrip.addProperty("fromlatitude", trip.fromLatitude)
        jsonTrip.addProperty("fromlongitude", trip.fromLongitude)
        jsonTrip.addProperty("tolatitude", trip.toLatitude)
        jsonTrip.addProperty("tolongitude", trip.toLongitude)

        return jsonTrip
    }
}