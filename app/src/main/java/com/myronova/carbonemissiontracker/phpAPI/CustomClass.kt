package com.myronova.carbonemissiontracker.phpAPI

import com.myronova.carbonemissiontracker.tripDB.Trip

data class CustomClass(val baseUrl:String, var trip: Trip, var bearerToken:String)