package com.myronova.carbonemissiontracker.phpAPI

import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.myronova.carbonemissiontracker.tripDB.Trip
import java.io.*
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

open class AllTrips : AsyncTask<String, Void, List<Trip>?>() {  // params, progress, result

    companion object {
        private const val TAG = "AllTrips"
        private const val NETIOBUFFER = 1024
    }

    // runs in background (not in UI thread)
    override fun doInBackground(vararg params: String): List<Trip>? {
        // params comes from the execute() call: params[0] is the url.
        try {
            val baseUrl = params[0]
            val apiKey = params[1]
            return downloadUrl(baseUrl, apiKey)
        } catch (e: IOException) {
            Log.e(TAG, "exception thrown by download", e)
            return null;
        }
    }

    /*
    * Given a URL, establishes an HttpUrlConnection and retrieves the web page
    * content as a InputStream, which it returns as a string.
    */
    @Throws(IOException::class)
    private fun downloadUrl(baseUrl: String, apiKey: String): List<Trip>? {
        val loginURL = "$baseUrl/api/v1/alltrips"
        Log.d(TAG, "downloadUrl $loginURL")

        var stream: InputStream? = null
        // Only read the first 500 characters of the retrieved
        // web page content.
        // int len = MAXBYTES;

        val url: URL
        try {
            url = URL(loginURL)
        } catch (e: MalformedURLException) {
            Log.d(TAG, e.message.toString())
            return null
        }

        var connection: HttpURLConnection? = null
        try {
            // create and open the connection
            connection = url.openConnection() as HttpURLConnection

            /*
             * set maximum time to wait for stream read read fails with
             * SocketTimeoutException if elapses before connection established
             *
             * in milliseconds
             *
             * default: 0 - timeout disabled
             */
            connection.readTimeout = 10000
            /*
             * set maximum time to wait while connecting connect fails with
             * SocketTimeoutException if elapses before connection established
             *
             * in milliseconds
             *
             * default: 0 - forces blocking connect timeout still occurs but
             * VERY LONG wait ~several minutes
             */
            connection.connectTimeout = 15000
            /*
             * HTTP Request method defined by protocol
             * GET/HEAD/POST/PUT/DELETE/TRACE/CONNECT
             *
             * default: GET
             */
            connection.requestMethod = "GET"
            // specifies whether this connection allows receiving data
            connection.doInput = true

            connection.addRequestProperty("Authorization", "Bearer $apiKey")
            connection.connect();

            val response = connection.responseCode
            Log.d(TAG, "Server returned: $response")

            /*
             *  check the status code HTTP_OK = 200 anything else we didn't get what
             *  we want in the data.
             */
            if (response != HttpURLConnection.HTTP_OK)
                return null;

            // get the stream for the data from the website
            stream = connection.inputStream
            // read the stream, returns String
            val responseStr = readIt(stream)
            Log.d(TAG, responseStr);

            val trips = fromJsonStr(responseStr)
            return trips

        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            Log.getStackTraceString(e)
            throw e
        } finally {
            /*
             * Make sure that the InputStream is closed after the app is finished using it.
             * Make sure the connection is closed after the app is finished using it.
             */

            if (stream != null) {
                try {
                    stream.close()
                } catch (ignore: IOException) {
                }

                if (connection != null)
                    try {
                        connection.disconnect()
                    } catch (ignore: IllegalStateException) {
                    }

            }
        }
    }

    /*
    * Reads stream from HTTP connection and converts it to a String.
    *
    *  We use a BufferedInputStream to read the data from the http connection InputStream
    *  into our buffer  (NETIOBUFFER bytes at a time, I often choose 1KiB for
    *  probably small datastreams.)
    *
    *  We then use a ByteArrayOutputStream + DataOutputStream.writer to collect the data
    *  and write it to a byte array.
    *  The reason this is done is that we do not know the length of the incoming data
    *  So the OutputStream components allow us to put data into and grow the buffer.
    *
    *  It is unfortunate that in Java everything is a pointer but we do not have access
    *  to the underlying structures.  If this is done in c or c++ we allocate, manage,
    *  grow and release the buffers as we need them.
    *
    *  In java using these classes has that effect.  It is not necessarily efficient,
    *  a better solution may be needed for large data exchange.
    */
    @Throws(IOException::class)
    fun readIt(stream: InputStream?): String {
        var bytesRead: Int
        var totalRead = 0
        val buffer = ByteArray(NETIOBUFFER)

        // for data from the server
        val bufferedInStream = BufferedInputStream(stream!!)
        // to collect data in our output stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(byteArrayOutputStream)

        // read the stream until end
        bytesRead = 0
        while (bytesRead != -1) {
            bytesRead = bufferedInStream.read(buffer)
            if (bytesRead > 0) {
                writer.write(buffer, 0, bytesRead)
                totalRead += bytesRead
            }
        }
        writer.flush()
        Log.d(
            TAG, "Bytes read: " + totalRead
                    + "(-1 means end of reader so max of)"
        )

        return byteArrayOutputStream.toString()
    }

    private fun fromJsonStr(responseStr: String): List<Trip>? {
        val gsonBuilder = GsonBuilder()

        val deserializer = TripJsonDeserializer()
        gsonBuilder.registerTypeAdapter(Trip::class.java, deserializer)

        val customGson: Gson = gsonBuilder.create()

        val tripListType: Type = object : TypeToken<ArrayList<Trip>?>() {}.type

        return customGson.fromJson<ArrayList<Trip>?>(responseStr, tripListType)
    }
}