package com.myronova.carbonemissiontracker

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.myronova.carbonemissiontracker.models.Fact
import kotlinx.android.synthetic.main.activity_sustainability_facts.*
import kotlin.random.Random

class SustainabilityFacts : AppCompatActivity() {

    companion object {
        const val TAG = "Sustainability_Facts"
    }

    private var firebaseDB: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var firebaseImageStorage: FirebaseStorage = FirebaseStorage.getInstance()

    private var facts = arrayListOf<Fact>()

    private var randIndex: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sustainability_facts)

        if(randIndex == -1){
            randIndex = savedInstanceState?.getInt("randIndex") ?: -1
        }
    }

    private fun createRandomIndex() {
        if (facts.size == 0) {
            return
        }
        randIndex = Random.nextInt(0, facts.size)
    }

    /**
     * This method creates a random number
     * uses this random number to get the fact that has that random number in database
     * and then sets the elements in the view to the received fact
     *
     */
    private fun getFact() {
        if (facts.size == 0) {
            return
        }

        val fact = facts[randIndex]

        factText.text = fact.fact
        var link = fact.url
        var imgUrl = fact.img
        if (imgUrl != null) {
            readImage(imgUrl)
        }
        Log.d(TAG, "this is the link $link")
        /**
         * handle the button in the view
         * when click on that button the web source gets open
         */
        factSrcBtn.setOnClickListener() {
            val intent = Intent(Intent(Intent.ACTION_VIEW))
            intent.data = Uri.parse(link)
            startActivity(intent)
        }
    }

    /**
     * This method receives the image url from firebase storage
     * @param imageURL
     */
    private fun readImage(imageURL: String) {
        val requestOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)
            // if an error or image can not be displayed
            .error(R.drawable.ic_launcher_background)

        Log.d(TAG, "Downloading storage url of image :$imageURL")

        var imageStorageRef = firebaseImageStorage.getReferenceFromUrl(imageURL)
        imageStorageRef.downloadUrl.addOnSuccessListener {
            val imageUri = it
            Log.d(TAG, "Storage url is$imageUri")
            if (imageUri != null) {
                Glide.with(this)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(imageUri)
                    .into(factImage)
            }
        }
    }

    /**
     * This method reads the firebase items
     */
    private fun readDescription() {
        firebaseDB.reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d(TAG, "number of dataSnapshot children is:" + dataSnapshot.children.count())

                dataSnapshot.child("Facts").children.mapNotNullTo(facts) { it.getValue<Fact>(Fact::class.java) }
                Log.d(TAG, "number of facts retrieve from firebase is : " + facts.size)

                if (facts.size == 0) {
                    return
                }
                if(randIndex == -1){
                    createRandomIndex()
                }
                getFact()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        readDescription()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        //saving a Bundle
        savedInstanceState.run {
            putInt("randIndex", randIndex)
        }
    }

}