package com.myronova.carbonemissiontracker

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.myronova.carbonemissiontracker.tripDB.TripViewModel
import kotlinx.android.synthetic.main.activity_trees_calculator.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.abs

class TreesCalculatorActivity : AppCompatActivity() {

    companion object {
        const val TAG = "TreesCalculatorActivity"
        const val TREE_CO2_PER_DAY:Float = 0.05753f
        const val permissionRequestCode = 300
        const val activityRequestCode = 400
    }

    private lateinit var tripViewModel: TripViewModel
    private var selectedEmailAddress = ""
    private var name = ""
    private var emails = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_trees_calculator)
        treesForAllTrips.text = "please wait..."

        getTotalEmission()
    }

    //As the activity begins to stop, the system calls this method
    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)

        //saving a Bundle
        savedInstanceState.run {
            putString("name", name)
            putStringArrayList("emails", emails)
        }
    }

    //Both the onCreate() and onRestoreInstanceState() callback methods receive the same Bundle that contains the instance state information.
    //The system calls this method AFTER the onStart() method.
    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        //restoring a Bundle
        name = savedInstanceState.getString("name", "")
        emails = savedInstanceState.getStringArrayList("emails") as ArrayList<String>

        showNameAndEmails()
    }

    private fun getTotalEmission() {
        tripViewModel = ViewModelProvider(this).get(TripViewModel::class.java)

        tripViewModel.totalEmission.observe(this, Observer { sum ->

            val s = sum ?: 0f
            val totalEmission = Math.round(s * 1000.0f) / 1000.0f

            Log.i(TAG, "This is the sum of co2 emitted $totalEmission kg")

            updateNumberOfTrees(totalEmission);
        })
        //it uses Lunch (not Async) - show and forget, do not assign to a variable
        tripViewModel.getTotalEmission()
    }

    private fun calculateDaysBetweenRegistrationAndCurrentDates() : Long{
        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            val registrationDate = Date(sharedPref.getString("RegistrationDate", ""))
            val currentDate = Date()

            val diffInMilSec = abs(currentDate.time - registrationDate.time)
            val numberOfDays = TimeUnit.DAYS.convert(diffInMilSec, TimeUnit.MILLISECONDS)

            Log.i(TAG, "This is number of days between two days $numberOfDays")

            return if (numberOfDays != 0L) numberOfDays else 1
        }
        return 1
    }

    private fun updateNumberOfTrees(totalEmission:Float){
        val numberOfDays = calculateDaysBetweenRegistrationAndCurrentDates()
        val average_CO2_per_day = totalEmission/numberOfDays
        val numberOfTrees = average_CO2_per_day/TREE_CO2_PER_DAY

        Log.i(TAG, "This is number of trees $numberOfTrees")

        treesForAllTrips.text = numberOfTrees.toInt().toString()
    }

    fun handleChoosePersonBtn(@Suppress("UNUSED_PARAMETER") view: View) {

        Log.i(TAG, "choosePersonBtn btn was clicked")

        // remove all previous radio buttons
        if (emailsRG != null) {
            emailsRG.removeAllViews()
        }
        checkReadContactsPermissions()
    }

    private fun checkReadContactsPermissions() {
        Log.i(TAG, "checkReadContactsPermissions")
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_CONTACTS
                ),
                permissionRequestCode
            )
        } else {
            // already permission granted
            getContactInfoUsingIntent()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            permissionRequestCode -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContactInfoUsingIntent()
                } else {
                    showToast(R.string.failGetPermission)
                }
            }
        }
    }

    private fun getContactInfoUsingIntent(){
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        startActivityForResult(intent, activityRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == activityRequestCode) {
            val contactData = data?.data
            val contactCursor = contentResolver.query(
                contactData!!,
                null,
                null,
                null,
                null)

            if (contactCursor!!.moveToFirst()) {
                // find name
                name =
                    contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))

                val contactId = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID))

                // find email
                val emailCursor = contentResolver.query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,
                    null,
                    null
                )
                while (emailCursor!!.moveToNext()) {
                    emails.add(emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)))
                }
                emailCursor.close()

                showNameAndEmails()

                Log.i(TAG, "name is $name, emails size is $emails.size")
            }
            contactCursor.close()
        }
    }

    private fun showNameAndEmails(){
        nameTV.text = "Name: $name"
        if (emails.size == 0)
        {
            showToast(R.string.noEmail)
        }
        else
        {
            for ((index, value) in emails.withIndex()){
                createAdditionalRadioButtons(index, value)
            }

            setChosenEmailAddress(emails)
        }
    }

    private fun createAdditionalRadioButtons(index: Int, email: String){

        val radioButton = RadioButton(this)
        radioButton.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        radioButton.text = email
        radioButton.textSize = 18F
        radioButton.id = index
        if (emailsRG != null) {
            emailsRG.addView(radioButton)
        }
    }

    private fun setChosenEmailAddress(emails: ArrayList<String>){
        emailsRG.setOnCheckedChangeListener { _, checkedId ->
                selectedEmailAddress = emails[checkedId]
                sendInfoBtn.isEnabled = true
        }
    }

    fun handleSendInfoBtn(@Suppress("UNUSED_PARAMETER") view: View) {

        Log.i(TAG, "sendInfoBtn btn was clicked")

        val intent = Intent(Intent.ACTION_SENDTO)
        //need this to prompts email
        intent.type = "message/rfc822"
        intent.data = Uri.parse("mailto:")
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>(selectedEmailAddress))
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject))
        intent.putExtra(
            Intent.EXTRA_TEXT,
            getString(R.string.treesForAllTripsMsg) + " \n" + treesForAllTrips.text
        )

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun showToast(msg: Int){
        val myToast =
            Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()
    }
}