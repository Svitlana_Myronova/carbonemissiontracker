package com.myronova.carbonemissiontracker

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class AboutActivity : AppCompatActivity() {

    companion object {
        const val TAG = "AboutActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.i(TAG, "start AboutActivity")
        setContentView(R.layout.activity_about)
    }
}