package com.myronova.carbonemissiontracker

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_settings.*
import org.json.JSONObject
import java.net.URL
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class SettingsActivity : AppCompatActivity() {

    companion object {
        const val TAG = "SettingActivity"
    }

    private var workLatitude: Float = 0.0f
    private var workLongitude: Float = 0.0f
    private var homeLatitude: Float = 0.0f
    private var homeLongitude: Float = 0.0f

    private lateinit var apiKeyHere: String
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        apiKeyHere = getString(R.string.apiKeyHere)
        setContentView(R.layout.activity_settings)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)

        Log.i(TAG, "Get info from shared preferences")

        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            editFirstName.setText(sharedPref.getString("FirstName", ""))
            editLastName.setText(sharedPref.getString("LastName", ""))
            editEMail.setText(sharedPref.getString("EMail", ""))
            editPassword.setText(sharedPref.getString("Password", ""))
            editPasswordConfirmation.setText(sharedPref.getString("PasswordConfirmation", ""))
            editRESTAPIURL.setText(
                sharedPref.getString(
                    "RESTAPIURL",
                    resources.getString(R.string.editRESTAPIURLText)
                )
            )
            editHomeAddress.setText(sharedPref.getString("HomeAddress", ""))
            editWorkAddress.setText(sharedPref.getString("WorkAddress", ""))
            textRegistrationDate.text = sharedPref.getString("RegistrationDate", "not registered")
        }
    }

    /**
     * Actions on a saveButton click
     */
    fun handleSaveBtn(@Suppress("UNUSED_PARAMETER") view: View) {
        Log.i(TAG, "Save btn was clicked")
        if (!validate()) {
            return
        }
        saveInfoToSharedPreferences()
        asyncResolveAndSaveHomeWorkLatLongToSharedPreferences()
    }

    /**
     * Show alert when user will press back button
     */
    override fun onBackPressed() {
        Log.i(TAG, "Back btn was clicked")

        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Alert")
            setMessage("Confirm discard?")
            setPositiveButton("OK", DialogInterface.OnClickListener {
                // _ - is a default parameter/parameter placeholder
                // we will not use _, _. we just need to call the function
                    _, _ ->
                super.onBackPressed()
            })
            setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, _ ->
                dialog.cancel()
            })
            show()
        }
    }

    /**
     * Used to save the info from settings activity to disk (SharedPreferences)
     */
    private fun saveInfoToSharedPreferences() {
        Log.i(TAG, "Saving info to shared preferences")

        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            val editor = sharedPref.edit()

            editor.putString("FirstName", editFirstName.text.toString())
            editor.putString("LastName", editLastName.text.toString())
            editor.putString("EMail", editEMail.text.toString())
            editor.putString("Password", editPassword.text.toString())
            editor.putString("PasswordConfirmation", editPasswordConfirmation.text.toString())
            editor.putString("RESTAPIURL", editRESTAPIURL.text.toString())
            editor.putString("HomeAddress", editHomeAddress.text.toString().trim())
            editor.putString("WorkAddress", editWorkAddress.text.toString().trim())

            if (!sharedPref.contains("RegistrationDate")) {
                editor.putString("RegistrationDate", Date().toString())
            }

            editor.commit()
        }
    }

    private fun asyncResolveAndSaveHomeWorkLatLongToSharedPreferences() {
        Log.i(TAG, "The value of the home address is : " + editHomeAddress.text.toString())
        Log.i(TAG, "The value of work address is : " + editWorkAddress.text.toString())
        //execute(Params... params)
        asyncTasks()
    }

    private fun asyncTasks() {
        progressBar.visibility = View.VISIBLE
        val taskCounter = AtomicInteger(2)
        HomeAsync(taskCounter).execute(editHomeAddress.text.toString().trim())
        WorkAsync(taskCounter).execute(editWorkAddress.text.toString().trim())
    }

    fun allTasksFinished() {
        progressBar.visibility = View.INVISIBLE

        //show toast message
        val myToast = Toast.makeText(applicationContext, R.string.saveMsg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()

        //finish settings activity
        finish()
    }

    /**
     * Check password and password confirmation match and check filling the form
     */
    private fun validate(): Boolean {

        val myToast: Toast

        if (editFirstName.text.toString().isEmpty() ||
            editLastName.text.toString().isEmpty() ||
            editEMail.text.toString().isEmpty() ||
            editPassword.text.toString().isEmpty() ||
            editRESTAPIURL.text.toString().isEmpty() ||
            editHomeAddress.text.toString().isEmpty() ||
            editWorkAddress.text.toString().isEmpty()
        ) {
            myToast = Toast.makeText(applicationContext, R.string.complitInfoMsg, Toast.LENGTH_LONG)
            myToast.setGravity(Gravity.CENTER, 0, 0)
            myToast.show()
            return false
        }
        if (editPassword.text.toString() != editPasswordConfirmation.text.toString()) {
            myToast = Toast.makeText(applicationContext, R.string.passwordMsg, Toast.LENGTH_SHORT)
            myToast.setGravity(Gravity.CENTER, 0, 0)
            myToast.show()
            return false
        }
        return true
    }

    /**
     * Converting home address to home latitude and home longitude
     */
    // AsyncTask<Params, Progress, Result>
    @SuppressLint("StaticFieldLeak")
    inner class HomeAsync(private val taskCounter: AtomicInteger) :
        AsyncTask<String, Void, String>() {
        /**
         * retrieves the json response as a string from the here api
         *
         * @param params
         * @return
         * */
        override fun doInBackground(vararg params: String?): String {
            var searchString = java.net.URLEncoder.encode(params[0], "utf-8")
            var homeResponse: String
            try {
                homeResponse =
                    URL("https://geocoder.ls.hereapi.com/6.2/geocode.json?searchtext=$searchString&gen=9&apiKey=$apiKeyHere").readText(
                        Charsets.UTF_8
                    )
                Log.i(TAG, "The response for Home is $homeResponse")
            } catch (e: Exception) {
                homeResponse = ""
                Log.e(TAG, "Failed to Fetch data $e")
            }
            return homeResponse
        }

        private fun processResponse(homeResult: String?) {
            if (homeResult == null || homeResult.isEmpty())
                return

            val jsonObj = JSONObject(homeResult)
            val views = jsonObj.getJSONObject("Response").getJSONArray("View")
            if (views.length() == 0)
                return
            var navPos = views.getJSONObject(0).getJSONArray("Result").getJSONObject(0)
                .getJSONObject("Location").getJSONArray("NavigationPosition").getJSONObject(0)

            homeLatitude = navPos.getString("Latitude").toFloat()
            homeLongitude = navPos.getString("Longitude").toFloat()

            // saving settings in the shared preferences
            val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
            if (sharedPref != null) {
                val editor = sharedPref.edit()
                editor.putFloat("homeLat", homeLatitude)
                editor.putFloat("homeLong", homeLongitude)
                editor.commit()
                Log.i(TAG, "home latitude: $homeLatitude")
                Log.i(TAG, "home longitude: $homeLongitude")
            }
        }

        /**
         * the method is called after the json response from the api has been retrieved
         * the method creates a json object of the response string and parse it to an array
         * it assigns the local variable a value
         *
         * @param homeResult
         * */
        override fun onPostExecute(homeResult: String?) {
            processResponse(homeResult)
            if (taskCounter.decrementAndGet() == 0) {
                allTasksFinished()
            }
        }
    }

    /**
     * Converting work address to work latitude and work longitude
     */
    // AsyncTask<Params, Progress, Result>
    @SuppressLint("StaticFieldLeak")
    inner class WorkAsync(private val taskCounter: AtomicInteger) :
        AsyncTask<String, Void, String>() {
        /**
         * retrieves the json response as a string from the here api
         *
         * @param params
         * @return
         * */
        override fun doInBackground(vararg params: String?): String {
            var searchString = java.net.URLEncoder.encode(params[0], "utf-8")
            var workResponse: String
            try {

                workResponse =
                    URL("https://geocoder.ls.hereapi.com/6.2/geocode.json?searchtext=$searchString&gen=9&apiKey=$apiKeyHere").readText(
                        Charsets.UTF_8
                    )

                Log.i(TAG, "The rsponse for Work is $workResponse")

            } catch (e: Exception) {
                workResponse = ""
                Log.e(TAG, "Failed to Fetch data $e")
            }
            return workResponse
        }

        private fun processResponse(workResult: String?) {
            if (workResult == null || workResult.isEmpty())
                return

            val jsonObj = JSONObject(workResult)
            val views = jsonObj.getJSONObject("Response").getJSONArray("View")
            if (views.length() == 0)
                return
            var navPos = jsonObj.getJSONObject("Response").getJSONArray("View").getJSONObject(0)
                .getJSONArray("Result").getJSONObject(0).getJSONObject("Location")
                .getJSONArray("NavigationPosition").getJSONObject(0)

            workLatitude = navPos.getString("Latitude").toFloat()
            workLongitude = navPos.getString("Longitude").toFloat()

            // saving settings in the shared preferences
            val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
            if (sharedPref != null) {
                val editor = sharedPref.edit()
                editor.putFloat("workLat", workLatitude)
                editor.putFloat("workLong", workLongitude)
                editor.commit()
                Log.i(TAG, "Work latitude: $workLatitude")
                Log.i(TAG, "Work longitude: $workLongitude")
            }
        }

        /**
         * the method is called after the json response from the api has been retrieved
         * the method creates a json object of the response string and parse it to an array
         * it assigns the local variable a value
         *
         * @param workResult
         * */
        override fun onPostExecute(workResult: String?) {
            processResponse(workResult)
            if (taskCounter.decrementAndGet() == 0) {
                allTasksFinished()
            }
        }
    }
}
