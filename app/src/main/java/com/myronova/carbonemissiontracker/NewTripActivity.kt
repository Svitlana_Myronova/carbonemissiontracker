package com.myronova.carbonemissiontracker

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.myronova.carbonemissiontracker.R.*
import kotlinx.android.synthetic.main.activity_new_trip.*
import org.json.JSONObject
import java.net.URL
import java.util.*

class NewTripActivity : AppCompatActivity() {

    companion object {
        const val TAG = "NewTripActivity"
    }

    private lateinit var apiKeyHere: String

    private var currentLat = 0.0f
    private var currentLong = 0.0f
    private var workLat = 0.0f
    private var workLong = 0.0f
    private var homeLong = 0.0f
    private var homeLat = 0.0f

    private var originAddress = ""
    private var originLatitude = 0.0f
    private var originLongitude = 0.0f

    private var destAddress = ""
    private var destLatitude = 0.0f
    private var destLongitude = 0.0f

    private var date = ""
    private var reasonTxt = ""
    private var mode = ""
    private var engine = ""
    private var volume = ""
    private var time = 0.0f
    private var distanceTravelled = 0.0f
    private var sharedRide = false
    private var carEmission = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        apiKeyHere = getString(R.string.apiKeyHere)
        setContentView(layout.activity_new_trip)

        getCurrentLocationFromIntent()

        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            workLat = sharedPref.getFloat("workLat", 0.0f)
            workLong = sharedPref.getFloat("workLong", 0.0f)
            homeLat = sharedPref.getFloat("homeLat", 0.0f)
            homeLong = sharedPref.getFloat("homeLong", 0.0f)
        }
        setDefaultValues()
    }

    private fun getCurrentLocationFromIntent() {
        currentLat = intent.getFloatExtra("currentLat", 0.0f)
        currentLong = intent.getFloatExtra("currentLong", 0.0f)
    }

    private fun setDefaultValues() {
        originAddress = "work"
        originLatitude = workLat
        originLongitude = workLong

        destAddress = "home"
        destLatitude = homeLat
        destLongitude = homeLong

        date = Date().run { toString() }
        mode = "car"
        engine = "gasoline"
        volume = "5.3"
    }

    fun onOriginRadioButton(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                id.radio_currentPosition ->
                    if (checked) {
                        originAddress = "currentPosition"
                        originLatitude = currentLat
                        originLongitude = currentLong
                    }
                id.radio_home ->
                    if (checked) {
                        originAddress = "home"
                        originLatitude = homeLat
                        originLongitude = homeLong
                    }
                id.radio_work ->
                    if (checked) {
                        originAddress = "work"
                        originLatitude = workLat
                        originLongitude = workLong
                    }
            }
        }
    }

    fun onDestinationRadioButton(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                id.radio_homeDest ->
                    if (checked) {
                        destAddress = "home"
                        destLatitude = homeLat
                        destLongitude = homeLong
                    }
                id.radio_workDest ->
                    if (checked) {
                        destAddress = "work"
                        destLatitude = workLat
                        destLongitude = workLong
                    }
            }
        }
    }

    fun onModeRadioButton(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                id.radio_car ->
                    if (checked) {
                        mode = "car"
                    }
                id.radio_carpool ->
                    if (checked) {
                        mode = "car"
                        sharedRide = true
                    }
                id.pubTransit ->
                    if (checked) {
                        mode = "publicTransport"
                    }
                id.pedestrian ->
                    if (checked) {
                        mode = "pedestrian"
                    }
            }
        }
    }

    fun handleSave(@Suppress("UNUSED_PARAMETER") view: View) {

        reasonTxt = reason.text.toString()

        if (reasonTxt.isBlank() || reasonTxt.isEmpty()) {
            //show toast message
            showToast(string.reasonError)
            return
        }

        TripDataAsync().execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class TripDataAsync() : AsyncTask<String, Void, String>() {

        override fun onPreExecute() {
            super.onPreExecute()
            progressBar.visibility = View.VISIBLE
        }

        /**
         * retrieves the json response as a string from the here api
         *
         * @param params
         * @return
         * */
        override fun doInBackground(vararg params: String?): String {
            var response: String
            try {
                response =
                    URL("https://route.ls.hereapi.com/routing/7.2/calculateroute.json?waypoint0=geo!$originLatitude,$originLongitude&waypoint1=geo!$destLatitude,$destLongitude&mode=fastest;$mode&vehicleType=$engine,$volume&traffic:disabled&apiKey=$apiKeyHere".trimMargin()).readText(
                        Charsets.UTF_8
                    )

                Log.i(TAG, "The response for route info $response")

            } catch (e: Exception) {
                response = ""
                Log.e(TAG, "Failed to Fetch data: $e.toString()")
            }
            return response
        }

        /**
         * the method is called after the json response from the api has been retrieved
         * the method creates a json object of the response string and parse it to an array
         * it assigns the local variable a value
         *
         * @param result
         * */
        override fun onPostExecute(result: String?) {
            if (result == null) {
                Log.i(TAG, "json response from the api is null")
                return
            }
            val jsonObj = JSONObject(result)
            var distance = jsonObj.getJSONObject("response").getJSONArray("route").getJSONObject(0)
                .getJSONObject("summary").getDouble("distance")
            distanceTravelled = distance.toFloat()

            var travelTime =
                jsonObj.getJSONObject("response").getJSONArray("route").getJSONObject(0)
                    .getJSONObject("summary").getInt("travelTime")
            time = travelTime.toFloat()

            var co2Emission =
                jsonObj.getJSONObject("response").getJSONArray("route").getJSONObject(0)
                    .getJSONObject("summary").getDouble("co2Emission")
            if (sharedRide) {
                carEmission = co2Emission.toFloat() / 3.0f
            } else {
                carEmission = co2Emission.toFloat()
            }
            if (mode == "publicTransport") {
                carEmission = co2Emission.toFloat() / 10.0f
            }
            if (mode == "pedestrian") {
                engine = "none"
                carEmission = 0f
            }

            Log.i(TAG, "The distance between Two coordinates is :  routes $distance")
            Log.i(TAG, " the Travel time is $travelTime")
            Log.i(TAG, "The Co2 produced  is $co2Emission")

            setDataToIntent()

            progressBar.visibility = View.INVISIBLE
        }
    }

    fun setDataToIntent() {
        val replyIntent = Intent()

        Log.i(TAG, "the origin latitude is $originLatitude and the longitude is $originLongitude")
        Log.i(TAG, "the destination latitude is $destLatitude and longitude is $destLongitude")

        replyIntent.putExtra("reason", reasonTxt)
        replyIntent.putExtra("mode", mode)
        replyIntent.putExtra("distance", distanceTravelled)
        replyIntent.putExtra("engine", engine)
        replyIntent.putExtra("originLat", originLatitude)
        replyIntent.putExtra("originLong", originLongitude)
        replyIntent.putExtra("destLat", destLatitude)
        replyIntent.putExtra("destLong", destLongitude)
        replyIntent.putExtra("carbon", carEmission)
        replyIntent.putExtra("date", date)
        replyIntent.putExtra("time", time)
        setResult(Activity.RESULT_OK, replyIntent)

        finish()
    }

    private fun showToast(msg: Int)
    {
        val myToast = Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()
    }
}

