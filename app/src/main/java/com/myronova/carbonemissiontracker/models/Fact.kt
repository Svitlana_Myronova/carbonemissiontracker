package com.myronova.carbonemissiontracker.models

data class Fact(
    var fact: String? = "DefaultValue",
    var img: String? = "DefaultValue",
    var url: String = "DefaultValue"
)