package com.myronova.carbonemissiontracker.tripDB

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Database access object for TripDB
 */
@Dao
interface TripDao {
    // insert trip
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTrip(trip: Trip)

    // get all trips
    @Query("SELECT * FROM trip_table")
    fun getAllTrips(): LiveData<List<Trip>>

    // get trip info
    @Query("SELECT * FROM trip_table WHERE id = :tripId LIMIT 1")
    suspend fun getTripInfo(tripId: Int): Trip

    // delete everything from trip_table
    @Query("DELETE FROM trip_table")
    suspend fun deleteAll()

    // get trip with the max id
    @Query("SELECT MAX(id) FROM trip_table")
    suspend fun getMaxId(): Int

    // get co2_emission of all trips
    @Query("SELECT SUM(co2_emission) FROM trip_table")
    suspend fun getTotalEmission(): Float
}