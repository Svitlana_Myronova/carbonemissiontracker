package com.myronova.carbonemissiontracker.tripDB

import androidx.room.TypeConverter
import java.util.Date

class TripDateConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}
