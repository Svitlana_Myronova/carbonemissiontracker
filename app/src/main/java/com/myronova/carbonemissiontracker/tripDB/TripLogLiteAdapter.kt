package com.myronova.carbonemissiontracker.tripDB

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.myronova.carbonemissiontracker.R

/**
 * Adapter populates the views in each row with the data.
 */
class TripLogLiteAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<TripLogLiteAdapter.ViewHolder>() {

    companion object {
        const val TAG = "TripLog"
    }

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var trips = mutableListOf<Trip>() // Cached copy of trips

    /**
     *  ViewHolder stores and recycles views as they are scrolled off screen
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tripItemView: TextView = itemView.findViewById(R.id.textView)
        val date: TextView = itemView.findViewById(R.id.date)
        val transport: TextView = itemView.findViewById(R.id.transport)
        val engine: TextView = itemView.findViewById(R.id.engine)
        val originLong: TextView = itemView.findViewById(R.id.originLong)
        val originLat: TextView = itemView.findViewById(R.id.originLat)
        val destLong: TextView = itemView.findViewById(R.id.destLong)
        val destLat: TextView = itemView.findViewById(R.id.destLat)
        val distance: TextView = itemView.findViewById(R.id.distance)
        val time: TextView = itemView.findViewById(R.id.timeTxt)
        val co2: TextView = itemView.findViewById(R.id.carbonEmitted)
    }

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.content_of_viewholder_triploglite, parent, false)
        return ViewHolder(itemView)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = trips[position]

        holder.tripItemView.text = current.reason
        holder.transport.text = "The travel mode: " + current.travelMode
        holder.date.text = current.date.toString()
        holder.engine.text = "The fuel: " + current.carEngine
        holder.originLat.text = current.fromLatitude.toString() + " from Latitude"
        holder.originLong.text = current.fromLongitude.toString() + " from Longitude"
        holder.destLat.text = current.toLatitude.toString() + " destination Latitude"
        holder.destLong.text = current.toLongitude.toString() + " destination Longitude"
        holder.distance.text = "The distance: " + (current.distanceKm / 1000).toString() + " km"
        holder.time.text = "The estimated time: " + current.travelTime / 60f + " min"
        holder.co2.text =
            " Amount of carbon emitted per the trip: " + current.co2Emission.toString() + " kg"
    }

    internal fun setTrips(trips: List<Trip>) {
        this.trips = trips.toMutableList()
        notifyDataSetChanged()
    }

    internal fun addTrip(trip: Trip) {
        this.trips.add(trip)
        notifyDataSetChanged()
    }

    // total number of rows
    override fun getItemCount() = trips.size
}