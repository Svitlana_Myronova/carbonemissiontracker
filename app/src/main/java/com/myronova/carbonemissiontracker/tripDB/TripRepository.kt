package com.myronova.carbonemissiontracker.tripDB

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Declares the DAO as a private property in the constructor. Pass in the DAO
 * instead of the whole database, because you only need access to the DAO
 */
class TripRepository(private val tripDao: TripDao) {
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allTrips: LiveData<List<Trip>> = tripDao.getAllTrips()
    val totalEmission: MutableLiveData<Float> = MutableLiveData()

    suspend fun getTripInfo(tripId: Int) {
        tripDao.getTripInfo(tripId)
    }

    suspend fun insert(trip: Trip) {
        tripDao.insertTrip(trip)
    }

    suspend fun getMaxId(): Int {
        return tripDao.getMaxId()
    }

    suspend fun getTotalEmission(): Float {
        return tripDao.getTotalEmission()
    }
}