package com.myronova.carbonemissiontracker.tripDB

import androidx.room.*
import androidx.room.Entity
import java.util.*

/**
 * Trip is a class that represents the SQLite table with different columns
 */
@Entity(tableName = "trip_table")
@TypeConverters(TripDateConverter::class)
data class Trip(
    @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "travel_mode") val travelMode: String,
    @ColumnInfo(name = "car_engine") val carEngine: String,
    @ColumnInfo(name = "from_latitude") val fromLatitude: Float,
    @ColumnInfo(name = "from_longitude") val fromLongitude: Float,
    @ColumnInfo(name = "to_latitude") val toLatitude: Float,
    @ColumnInfo(name = "to_longitude") val toLongitude: Float,
    @ColumnInfo(name = "distance_km") val distanceKm: Float,
    @ColumnInfo(name = "co2_emission") val co2Emission: Float,
    @ColumnInfo(name = "travel_time") val travelTime: Float,
    @ColumnInfo(name = "reason") val reason: String,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)
