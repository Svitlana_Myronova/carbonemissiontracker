package com.myronova.carbonemissiontracker.tripDB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

/**
 * Annotates class to be a Room Database with a table (entity) of the Trip class
 */
@Database(entities = [Trip::class], version = 1, exportSchema = false)
abstract class TripRoomDatabase : RoomDatabase() {

    abstract fun tripDao(): TripDao

    private class TripDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            // if instance of DB is not null, then populate it
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(
                        database.tripDao()
                    )
                }
            }
        }
    }

    companion object {
        // Singleton prevents multiple instances of database opening at the same time.
        @Volatile
        private var INSTANCE: TripRoomDatabase? = null

        //access to INSTANCE
        fun getDatabase(context: Context, scope: CoroutineScope): TripRoomDatabase {
            // if instance is null, then call databaseBuilder(...)
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TripRoomDatabase::class.java,
                    "word_database"
                )
                    .addCallback(
                        TripDatabaseCallback(
                            scope
                        )
                    )
                    .build()
                // initialization of INSTANCE for future calls
                INSTANCE = instance
                // return instance
                instance
            }
        }

        /**
         * Populate the database in a new coroutine.
         * If you want to start with some data.
         */
        suspend fun populateDatabase(tripDao: TripDao) {
            // Start the app with a clean database every time.
            // tripDao.deleteAll()
            // Not needed if you only populate on creation.
            val df = SimpleDateFormat("yyyy-MM-dd")
            val date = df.parse("2020-06-20") ?: return // return from function
            // an example
            var trip = Trip(
                Date(2020, 6, 20), "car", "diesel", 123f, 12343f, 12343f, 23424324f,
                55f, 30f, 123.4f, "Business Trip"
            )
            //tripDao.insertTrip(trip)
        }
    }

}