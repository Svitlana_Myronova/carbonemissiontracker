package com.myronova.carbonemissiontracker.tripDB

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// Class extends AndroidViewModel and requires application as a parameter.
class TripViewModel(application: Application) : AndroidViewModel(application) {

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: TripRepository

    // LiveData gives us updated words when they change.
    val allTrips: LiveData<List<Trip>>
    val totalEmission: MutableLiveData<Float> = MutableLiveData()

    init {
        // Gets reference to TripDao from TripRoomDatabase to construct the correct TripRepository.
        val tripsDao = TripRoomDatabase.getDatabase(application, viewModelScope).tripDao()
        repository = TripRepository(tripsDao)
        allTrips = repository.allTrips
    }

    /**
     * The implementation of insert() in the database is completely hidden from the UI.
     * Room ensures that you're not doing any long running operations on
     * the main thread, blocking the UI, so we don't need to handle changing Dispatchers.
     * ViewModels have a coroutine scope based on their lifecycle called
     * viewModelScope which we can use here.
     */
    fun insert(trip: Trip) = viewModelScope.launch {
        repository.insert(trip)
    }

    fun getTotalEmission() {
        viewModelScope.launch {
            totalEmission.value = repository.getTotalEmission()
        }
    }
}