package com.myronova.carbonemissiontracker

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.myronova.carbonemissiontracker.phpAPI.AddTrip
import com.myronova.carbonemissiontracker.phpAPI.AllTrips
import com.myronova.carbonemissiontracker.phpAPI.CustomClass
import com.myronova.carbonemissiontracker.phpAPI.LoginTask
import com.myronova.carbonemissiontracker.tripDB.Trip
import com.myronova.carbonemissiontracker.tripDB.TripLogLiteAdapter
import com.myronova.carbonemissiontracker.tripDB.TripViewModel
import kotlinx.android.synthetic.main.activity_trip_log.*
import kotlinx.android.synthetic.main.recyclerview.*
import java.util.*

class TripLogRemoteActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "TripLogRemoteActivity"
        const val activityRequestCode = 200
    }

    private lateinit var adapter: TripLogLiteAdapter
    private lateinit var tripViewModel: TripViewModel
    private var currentLat = 0.0f
    private var currentLong = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_log_remote)
        setSupportActionBar(toolbar)

        adapter = TripLogLiteAdapter(this)
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(this)

        // Get ViewModel from the ViewModelProvider.
        tripViewModel = ViewModelProvider(this).get(TripViewModel::class.java)

        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recyclerview.addItemDecoration(decoration)

        loginForGetAllTrips()

        getCurrentLocationFromIntent()

        floatActionBtn.setOnClickListener { _ ->
            val intent = Intent(this, NewTripActivity::class.java)
            intent.putExtra("currentLat", currentLat)
            intent.putExtra("currentLong", currentLong)
            startActivityForResult(intent, TripLogActivity.activityRequestCode)
        }
    }

    private fun getCurrentLocationFromIntent(){
        currentLat = intent.getFloatExtra("currentLat", 0.0f)
        currentLong = intent.getFloatExtra("currentLong", 0.0f)
    }

    private fun showToast(msg: Int){
        val myToast =
            Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()
    }

    @Suppress("DEPRECATION")
    fun isInternetConnected(): Boolean {
        // Check if we can get on the network
        val connMngr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMngr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            return true;
        }
        return false;
    }

    private fun getRESTAPIURL(): String? {
        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)

        val stringUrl = sharedPref.getString("RESTAPIURL", getString(R.string.editRESTAPIURLText))
        Log.d(TAG, "get url RESTful api $stringUrl")
        return stringUrl
    }

    private fun getEMail(): String? {
        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)

        val value = sharedPref.getString("EMail", "")
        Log.d(TAG, "get EMail $value")
        return value
    }

    private fun getPassword(): String? {
        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)

        val value = sharedPref.getString("Password", "")
        Log.d(TAG, "get Password $value")
        return value
    }

    private fun loginForGetAllTrips() {
        if (!isInternetConnected()) {
            showToast(R.string.noNetworkConMsg)
            return;
        }

        val loginTask = MyLoginTaskForGetAllTrips()
        val baseUrl = getRESTAPIURL()
        val email = getEMail()
        val password = getPassword()
        loginTask.execute(baseUrl, email, password)
    }

    @SuppressLint("StaticFieldLeak")
    private inner class MyLoginTaskForGetAllTrips : LoginTask() {
        override fun onPostExecute(apiKey: String?) {
            super.onPostExecute(apiKey)

            if (apiKey == null) {
                //toast that login failed
                showToast(R.string.noNetworkConMsg)
                return
            }

            getAllTrips(apiKey)
        }
    }

    fun getAllTrips(apiKey: String) {
        val allTrips = MyAllTrips()
        val baseUrl = getRESTAPIURL()
        allTrips.execute(baseUrl, apiKey)
    }

    private inner class MyAllTrips : AllTrips() {
        override fun onPostExecute(result: List<Trip>?) {
            super.onPostExecute(result)

            adapter.setTrips(result ?: arrayListOf())
        }
    }

    private fun loginForAddTrip(trip: Trip) {
        if (!isInternetConnected()) {
            showToast(R.string.noNetworkConMsg)
            return;
        }

        val loginTask = MyLoginTaskForAddTrip(trip)
        val baseUrl = getRESTAPIURL()
        val email = getEMail()
        val password = getPassword()
        loginTask.execute(baseUrl, email, password)
    }

    @SuppressLint("StaticFieldLeak")
    private inner class MyLoginTaskForAddTrip(val trip:Trip) : LoginTask() {
        override fun onPostExecute(apiKey: String?) {
            super.onPostExecute(apiKey)

            if (apiKey == null) {
                //toast that login failed
                showToast(R.string.noNetworkConMsg)
                return
            }

            setTrip(trip, apiKey)
        }
    }

    fun setTrip(trip:Trip, apiKey: String) {
        val addTrip = MyAddTrip()
        val baseUrl = getRESTAPIURL()?: ""
        val customClass = CustomClass(baseUrl, trip, apiKey)
        addTrip.execute(customClass)
    }

    private inner class MyAddTrip : AddTrip() {
        override fun onPostExecute(result: Trip?) {
            super.onPostExecute(result)
            if(result!=null) {
                adapter.addTrip(result)
            }
        }
    }

    /**
     * This function will handle the data returned from an activity
     * and it will verify it's status code. this function is used also to create
     * a trip object
     *
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == activityRequestCode && resultCode == Activity.RESULT_OK) {

            val reason = intentData!!.getStringExtra("reason")?: ""
            var engine = intentData.getStringExtra("engine")?: ""
            val distance = intentData.getFloatExtra("distance", 0f)
            val mode = intentData.getStringExtra("mode")?: ""
            val originLat = intentData.getFloatExtra("originLat", 0f)
            val originLong = intentData.getFloatExtra("originLong", 0f)
            val destLat = intentData.getFloatExtra("destLat", 0f)
            val destLong = intentData.getFloatExtra("destLong", 0f)
            val carbon = intentData.getFloatExtra("carbon", 0f)
            val time = intentData.getFloatExtra("time", 0f)
            val dateTxt = intentData.getStringExtra("date")

            Log.i(TripLogActivity.TAG, "received origin LAt is $originLat and origin long is $originLong")
            Log.i(TripLogActivity.TAG, "received destination is destination lat is $destLat and long is $destLong")
            var date = Date(dateTxt)
            val trip = Trip(
                date,
                mode,
                engine,
                originLat,
                originLong,
                destLat,
                destLong,
                distance,
                carbon,
                time,
                reason
            )
            loginForAddTrip(trip)
        } else {
            showToast(R.string.empty_not_saved)
        }
    }
}