package com.myronova.carbonemissiontracker

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_weather.*
import org.json.JSONObject
import java.net.URL

class WeatherActivity : AppCompatActivity() {

    companion object {
        const val TAG = "WeatherActivity"
    }

    private var currentLong: Float = 0.0f
    private var currentLat: Float = 0.0f

    private lateinit var apiKeyHere: String
    private lateinit var apiKeyWeather: String


    /**
     * a lifecycle method called when the application starts
     * @param savedInstanceState
     * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        apiKeyHere = getString(R.string.apiKeyHere)
        apiKeyWeather = getString(R.string.apiKeyWeather)

        setContentView(R.layout.activity_weather)

        getCurrentLocationFromIntent()

        WeatherAsynchTask().execute()
        UVIAsynchTask().execute()
    }

    private fun getCurrentLocationFromIntent() {
        currentLat = intent.getFloatExtra("currentLat", 0.0f)
        currentLong = intent.getFloatExtra("currentLong", 0.0f)
    }

    /**
     * WeatherAsynchTask is class that implements AsyncTask interface
     *
     * */
    @SuppressLint("StaticFieldLeak")
    inner class WeatherAsynchTask() : AsyncTask<String, Void, String>() {

        private fun String.toDouble(): Double = java.lang.Double.parseDouble(this)

        /**
         * retrieves the json response as a string from the here api
         *
         * @param params
         * @return
         * */
        override fun doInBackground(vararg params: String?): String {
            var response: String

            try {
                response =
                    URL("https://weather.ls.hereapi.com/weather/1.0/report.json?product=forecast_hourly&latitude=$currentLat&longitude=$currentLong&oneobservation=true&language=en&apiKey=$apiKeyHere").readText(
                        Charsets.UTF_8
                    )
            } catch (e: Exception) {
                response = ""
            }
            return response
        }

        /**
         * the method is called after the json response from the api has been retrieved
         * the method creates a json object of the response string and parse it to an array
         * it assigns the local string variables their values and sets the text of the textViews
         *
         * @param result
         * */
        @SuppressLint("SetTextI18n")
        override fun onPostExecute(result: String) {
            val typeOfTransportation: String

            if (result.isEmpty()) {
                info.text = getString(R.string.connectionError)
                return
            }
            val jsonObj = JSONObject(result)

            val forecast =
                jsonObj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation")
                    .getJSONArray("forecast")

            //current temperature and description
            val currentTemp = forecast.getJSONObject(0).getString("temperature")
            val currentDesc = forecast.getJSONObject(0).getString("description")

            if (currentTemp.toDouble() <= -5 || currentDesc.contains(
                    "snow",
                    ignoreCase = true
                ) || currentDesc.contains("rain", ignoreCase = true)
            ) {
                typeOfTransportation = getString(R.string.takePublicTrns)
            } else if (currentTemp.toDouble() < 10 && currentTemp.toDouble() > -5) {
                typeOfTransportation = getString(R.string.takePublicTrns_walk)
            } else if (currentTemp.toDouble() < 30 && currentTemp.toDouble() > 10) {
                typeOfTransportation = getString(R.string.bike_walk)
            } else {
                typeOfTransportation = getString(R.string.stayPlace)
            }
            Log.d(TAG, "temp: $currentTemp")

            //get the current temperature which is the first in the json array
            temp.text = getString(R.string.temperatureCelcius, currentTemp)

            weather.text = currentDesc

            info.text = jsonObj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation")
                .getString("city") + ", " + jsonObj.getJSONObject("hourlyForecasts")
                .getJSONObject("forecastLocation").getString("state")

            transport.text = getString(R.string.proposition, typeOfTransportation)
        }

    }

    /**
     * UVIAsynchTask is class that implements AsyncTask interface and call the openweathermap API
     * to retrieve the uv index for the current weather
     *
     * */
    @SuppressLint("StaticFieldLeak")
    inner class UVIAsynchTask() : AsyncTask<String, Void, String>() {

        private fun String.toDouble(): Double = java.lang.Double.parseDouble(this)

        /**
         * retrieves the json response as a string from the open weather map api
         *
         * @param p0
         * @return
         * */
        override fun doInBackground(vararg p0: String?): String {
            var response: String
            try {
                response =
                    URL("https://api.openweathermap.org/data/2.5/uvi?appid=$apiKeyWeather&lat=$currentLat&lon=$currentLong").readText(
                        Charsets.UTF_8
                    )
            } catch (e: Exception) {
                response = ""
            }
            return response
        }

        /**
         * the method is called after the json response from the api has been retrieved
         * the method creates a json object of the response string and parse it to an array
         * it assigns the local string variables their values and sets the text of the textViews
         *
         * @param result
         * */
        override fun onPostExecute(result: String) {

            if (result.isEmpty()) {
                uvi.text = getString(R.string.connectionError)
                return
            }
            val jsonObj = JSONObject(result)

            //ultraviolet rays' index
            val uvIndex = jsonObj.getString("value")

            lateinit var uvIndexText: String
            if (uvIndex.toDouble() >= 0 && uvIndex.toDouble() < 3) {
                uvIndexText = getString(R.string.uvIndexLow)
            } else if (uvIndex.toDouble() >= 3 && uvIndex.toDouble() < 6) {
                uvIndexText = getString(R.string.uvIndexModerate)
            } else if (uvIndex.toDouble() >= 6 && uvIndex.toDouble() < 8) {
                uvIndexText = getString(R.string.uvIndexHigh)
            } else if (uvIndex.toDouble() >= 8 && uvIndex.toDouble() < 11) {
                uvIndexText = getString(R.string.uvIndexVeryHigh)
            } else {
                uvIndexText = getString(R.string.uvIndexExtreme)
            }

            uvi.text = getString(R.string.uvIndexDesc, uvIndexText, uvIndex)

            Log.d(TAG, "uvi value $uvIndex")
        }
    }
}