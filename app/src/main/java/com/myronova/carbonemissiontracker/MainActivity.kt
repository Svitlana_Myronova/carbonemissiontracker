package com.myronova.carbonemissiontracker

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.myronova.carbonemissiontracker.tripDB.TripViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private val locationRequestCode = 1000
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var currentLat = 0.0f
    private var currentLong = 0.0f

    private lateinit var tripViewModel: TripViewModel

    private lateinit var locationCallback: LocationCallback

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            locationRequestCode -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates()
                }
            }
        }
    }

    private fun startLocationUpdates() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        var locationRequest: LocationRequest? = createLocationRequest() ?: return
        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } catch (e: SecurityException) {
            showToast(R.string.permission_rejected)
        }
    }

    private fun getCurrentLocation() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    currentLat = location.latitude.toFloat()
                    currentLong = location.longitude.toFloat()
                }
                Log.i(
                    TAG,
                    " Current location Latitude is $currentLat and Longitude is $currentLong"
                )
            }
        }
    }

    private fun checkLocationPermissions() {
        Log.i(TAG, "checkLocationPermissions")
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                locationRequestCode
            )

        } else {
            // already permission granted
            startLocationUpdates()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // if user is not registered, start settings activity
        val sharedPref = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (!sharedPref.contains("RegistrationDate")) {
            Log.i(TAG, "start SettingActivity")
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        getCurrentLocation()

        updateTotalEmission()
    }

    private fun updateTotalEmission() {
        tripViewModel = ViewModelProvider(this).get(TripViewModel::class.java)

        tripViewModel.totalEmission.observe(this, Observer { sum ->

            val s = sum ?: 0f
            val sRound = Math.round(s * 1000.0f) / 1000.0f

            Log.i(TAG, "This is the sum of co2 emitted $s kg")
            totalEmission.text = "Total CO2 emission for all trips: $sRound kg"
        })
        //it uses Lunch (not Async) - show and forget, do not assign to a variable
        tripViewModel.getTotalEmission()
    }

    override fun onResume() {
        super.onResume()

        checkLocationPermissions()
        tripViewModel.getTotalEmission()
    }

    private fun createLocationRequest(): LocationRequest? {
        val locationRequest = LocationRequest.create()?.apply {
            interval = 300000 // get gps location every 5 min
            fastestInterval = 5000 // set the distance value in meter
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        return locationRequest
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {

            R.id.action_settings -> run {
                Log.i(TAG, "action_settings")
                var settingsIntent: Intent = Intent(this, SettingsActivity::class.java)
                startActivity(settingsIntent)
                return true
            }

            R.id.action_WWW -> run {
                Log.i(TAG, "action_WWW")
                var browserIntent: Intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.google.com")
                )
                startActivity(browserIntent)
                return true
            }

            R.id.action_about -> run {
                Log.i(TAG, "action_about")
                var aboutIntent: Intent = Intent(this, AboutActivity::class.java)
                startActivity(aboutIntent)
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    //start Trip Log Activity
    fun handleSqliteTripLog(@Suppress("UNUSED_PARAMETER") view: View) {
        if(currentLat == 0.0f || currentLong == 0.0f){
            showToast(R.string.currentLocation_not_received)
            return
        }
        Log.i(TAG, "tripLogSQLite btn was clicked")
        val intent = Intent(this, TripLogActivity::class.java)
        intent.putExtra("currentLat", currentLat)
        intent.putExtra("currentLong", currentLong)
        startActivity(intent)
    }

    //start Trip Log Remote Activity
    fun handleTripLogRemote(@Suppress("UNUSED_PARAMETER") view: View) {
        if(currentLat == 0.0f || currentLong == 0.0f){
            showToast(R.string.currentLocation_not_received)
            return
        }
        Log.i(TAG, "tripLogRemote btn was clicked")
        val intent = Intent(this, TripLogRemoteActivity::class.java)
        intent.putExtra("currentLat", currentLat)
        intent.putExtra("currentLong", currentLong)
        startActivity(intent)
    }

    //start Weather Activity
    fun handleWeather(@Suppress("UNUSED_PARAMETER") view: View) {
        if(currentLat == 0.0f || currentLong == 0.0f){
            showToast(R.string.currentLocation_not_received)
            return
        }
        Log.i(TAG, "weather btn was clicked")
        val intent = Intent(this, WeatherActivity::class.java)
        intent.putExtra("currentLat", currentLat)
        intent.putExtra("currentLong", currentLong)
        startActivity(intent)
    }

    //start SustainabilityFacts Activity
    fun handleFacts(@Suppress("UNUSED_PARAMETER") view: View) {
        Log.i(TAG, "facts btn was clicked")
        val intent = Intent(this, SustainabilityFacts::class.java)
        startActivity(intent)
    }

    //start Co2 Calculation Activity
    fun handleTreesCalculation(@Suppress("UNUSED_PARAMETER") view: View) {
        Log.i(TAG, "calculator btn was clicked")
        val intent = Intent(this, TreesCalculatorActivity::class.java)
        startActivity(intent)
    }

    private fun showToast(msg: Int){
        val myToast =
            Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()
    }

}
