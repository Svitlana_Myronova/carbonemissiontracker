package com.myronova.carbonemissiontracker

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.myronova.carbonemissiontracker.tripDB.Trip
import com.myronova.carbonemissiontracker.tripDB.TripLogLiteAdapter
import com.myronova.carbonemissiontracker.tripDB.TripViewModel
import kotlinx.android.synthetic.main.activity_trip_log.*
import kotlinx.android.synthetic.main.recyclerview.*
import java.util.*

class TripLogActivity : AppCompatActivity() {

    companion object {
        const val TAG = "TripLog"
        const val activityRequestCode = 200
    }

    private lateinit var tripViewModel: TripViewModel
    private var currentLat = 0.0f
    private var currentLong = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_log)
        setSupportActionBar(toolbar)

        val adapter = TripLogLiteAdapter(this)
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(this)

        // Get ViewModel from the ViewModelProvider.
        tripViewModel = ViewModelProvider(this).get(TripViewModel::class.java)

        // Add an observer on the LiveData.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        tripViewModel.allTrips.observe(this, Observer { trips ->
            // Update the cached copy of the words in the adapter.
            trips?.let { adapter.setTrips(it) }
        })

        getCurrentLocationFromIntent()

        floatActionBtn.setOnClickListener { _ ->
            val intent = Intent(this, NewTripActivity::class.java)
            intent.putExtra("currentLat", currentLat)
            intent.putExtra("currentLong", currentLong)
            startActivityForResult(intent, activityRequestCode)
        }
    }

    private fun getCurrentLocationFromIntent(){
        currentLat = intent.getFloatExtra("currentLat", 0.0f)
        currentLong = intent.getFloatExtra("currentLong", 0.0f)
    }

    /**
     * This function will handle the data returned from an activity
     * and it will verify it's status code. this function is used also to create
     * a trip object and add it to the database
     *
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == activityRequestCode && resultCode == Activity.RESULT_OK) {

            val reason = intentData!!.getStringExtra("reason")?: ""
            val engine = intentData.getStringExtra("engine")?: "gasoline"
            val distance = intentData.getFloatExtra("distance", 0f)
            val mode = intentData.getStringExtra("mode")?: "car"
            val originLat = intentData.getFloatExtra("originLat", 0f)
            val originLong = intentData.getFloatExtra("originLong", 0f)
            val destLat = intentData.getFloatExtra("destLat", 0f)
            val destLong = intentData.getFloatExtra("destLong", 0f)
            val carbon = intentData.getFloatExtra("carbon", 0f)
            val time = intentData.getFloatExtra("time", 0f)
            val dateTxt = intentData.getStringExtra("date")

            Log.i(TAG, "received origin LAt is $originLat and origin long is $originLong")
            Log.i(TAG, "received destination is destination lat is $destLat and long is $destLong")
            var date = Date(dateTxt)
            val trip = Trip(
                date,
                mode,
                engine,
                originLat,
                originLong,
                destLat,
                destLong,
                distance,
                carbon,
                time,
                reason
            )
            tripViewModel.insert(trip)
        } else {
            showToast(R.string.empty_not_saved)
        }
    }

    private fun showToast(msg: Int){
        val myToast =
            Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER, 0, 0)
        myToast.show()
    }
}