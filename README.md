# Carbon Emission Tracker

Carbon Emission Tracker is an app for Android. It is written in Kotlin using Android Studio.
This project is about calculating the end-user’s CO2 emissions stemming from transport to/from work. It presents trips with different amounts of emissions generated through use of cars, public transport and walking/biking. Carbon Emission Tracker app shows weather and ultraviolet index for current location using **HERE REST API** and **Weather APIs**. It selects a random fact from **Firebase** database. Project shows/adds trips to **SQLite** and **remote databases**. It **sends the info via email**.

## MainActivity

It is the starting point of the app. MainActivity contains **Menu**, 5 image buttons (**Weather**, **Sustainability Facts**, **Trip Log Remote**, **Trip Log** and **Trees Calculator**) and **Total CO2 emission** for all local trips. It checks/requests location permission and gets the current location latitude and longitude using **LocationCallback**.

![Screenshort](Capture1.PNG)

## Menu

It consists of **Settings**, **About** and **Google** menu items.
-	**Settings** creates/shows/updates any settings saved for the app. If the user is not registered yet on this app, he/she gets here first. Settings convetrs home and work addresses into homeLongitude/homeLatitude and workLongitude/workLatitude using [HERE REST APIs](https://developer.here.com/develop/rest-apis).
-	**About** describes the app.
-	**Google** lunches Google web page.

## Weather

It gets the current weather condition using [HERE REST APIs](https://developer.here.com/develop/rest-apis) and the ultraviolet index using [Weather API](https://openweathermap.org/api) for current location. Depending on the received data it suggests the type of transportation.

![Screenshort](Capture2.PNG)

## Sustainability Facts

It selects a random fact from the **Firebase** database and displays it. **Glide** (image loading library for Android) loads the image into ImageView. The button "Show the source" opens the web source of the fact.

![Screenshort](Capture3.PNG)

## Trip Log

It shows all trips from the **SQLite** database using **RecyclerView**. Floating action button allows to create a trip.  TripLogActivity adds the new trip to the SQLite database.

![Screenshort](Capture5.PNG)

## Trip Log Remote 

It shows all trips from the **Remote database** using **RecyclerView**. Remote database [carbontrackerteam06.herokuapp.com](https://carbontrackerteam06.herokuapp.com) is a Laravel website hosted on [Heroku: Cloud Application Platform](https://heroku.com). Floating action button allows to create a new trip. TripLogRemoteActivity adds the new trip to the remote database.

![Screenshort](Capture4.PNG)

## Trees Calculator

Using the formula "one mature tree absorbs carbon dioxide at a rate of 48 lbs/year = 0.0218 metric tons/year" it shows how many trees are required to offset the CO2 for all trips and **sends this info via email**.

![Screenshort](Capture6.PNG)
        
## Total CO2 emission for all trips

It shows/updates CO2 emission for all local trips from **SQLite** database.